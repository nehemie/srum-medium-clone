import React from 'react';

/**
 * Import the router
 */
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
/**
 * Import the main pages
 */
import HomePage from './App/Pages/HomePage/HomePage';
import ArticleViewPage from './App/Pages/ArticleViewPage';
/**
 * Component declaration
 */
function App() {
  return (
    <div className="App">
      <Router>
        <Switch>

          {/*  */}
          <Route path="/article/:id">
            <ArticleViewPage />
          </Route>
          {/*  */}
          <Route match="/">
            <HomePage />
          </Route>
          {/*  */}
        </Switch>
      </Router>
    </div>
  );
}

export default App;
