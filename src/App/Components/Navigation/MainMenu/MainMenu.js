import React from 'react';
import "./MainMenu.less";
import MainMenuAccount from "./MenuAccount";
import Logo from "../../../../Assets/logo.svg"
import { Menu, Icon } from "antd"

function Component() {
    return (
        <div className="MainMenu">
            <div className='MainMenu-Logo'>
                <img src={Logo} />
            </div>
            <div>
                <div>
                    <Menu mode="horizontal">
                        <Menu.Item>
                            <Icon type="home" />
                            Home</Menu.Item>
                        <Menu.Item>
                            <Icon type="file-text" />
                            Artciles</Menu.Item>
                        <Menu.Item>
                            <Icon type="setting" />
                            Settings</Menu.Item>
                    </Menu>
                </div>
            </div>
            <div>
                <MainMenuAccount />
            </div>

        </div>
    );
}

export default Component;
