import React from 'react';
import { Avatar } from 'antd';

function Component() {
    return (
        <div className="MainMenu-Account">
            <div>
                <Avatar size={40} icon="user" />
            </div>
            <div className="MainMenu-Account-D">
                <h3>John Doe</h3>
                <div>
                    <span>
                        Kigali Rwanda
                    </span>
                </div>
            </div>
        </div>
    );
}

export default Component;
