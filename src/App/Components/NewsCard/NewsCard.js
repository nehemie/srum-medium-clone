import React from 'react';
import './NewsCard.less';
/**
 * Ant componenrs
 */
import { Layout, Icon, Button } from 'antd';


/**
 * Export component
 */
export default function (props) {
    console.log(props.article.author.name)
    return (
        <div className="NCard-H">
            <div className="NCard">
                <div className="NCard-Top">
                    <img src={props.article.img} />
                    <div className="NCard-Top-Ov">
                        <div className="NCard-Auth">
                            <h4>{props.article.author.name}</h4>
                            <div>{props.article.date}</div>
                            <div>{props.article.claps}</div>
                        </div>
                        <div className="NCard-Actions">
                            <Button type="link" shape="circle" icon="share" />
                            <Button type="link" shape="circle" icon="clap" />
                            <Button type="link" shape="circle" icon="ellipsis" />
                        </div>
                    </div>
                </div>
                <div className="NCard-Bottom">
                    <div>
                        <h2>{props.article.title}</h2>
                        <p>
                            {props.article.txt}
                        </p>
                    </div>
                </div>
            </div>
        </div>
    )
}