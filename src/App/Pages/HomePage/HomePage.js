import React from 'react';
import "./HomePage.less";
import img1 from './../../../Assets/articles/bank-phrom-Tzm3Oyu_6sk-unsplash.jpg';
import img2 from './../../../Assets/articles/carl-raw-m3hn2Kn5Bns-unsplash.jpg';
import img3 from './../../../Assets/articles/florian-olivo-Mf23RF8xArY-unsplash.jpg';
import img4 from './../../../Assets/articles/harrison-moore-zBG29d6Z98I-unsplash.jpg';
import img5 from './../../../Assets/articles/tyler-nix-tPz-uoz5RBU-unsplash.jpg';
import img6 from './../../../Assets/articles/tim-foster-y6uilkrSyMs-unsplash.jpg';
import img7 from './../../../Assets/articles/nicholas-doherty-pONBhDyOFoM-unsplash.jpg';
/**
 * Page components
 */
import MainMenu from "../../Components/Navigation/MainMenu/MainMenu"
import MainFooter from "../../Components/Navigation/MainFooter/MainFooter"

import NCard from './../../Components/NewsCard/NewsCard';


import StackGrid from "react-stack-grid";
/**
 * Ant componenrs
 */
import { Layout, Icon, Button, Row, Col } from 'antd';
const { Header, Footer, Sider, Content } = Layout;

/**
 * 
 */

/**
 * Export component
 */
function HomePage() {
    const categories = [
        'All',
        'Technology',
        'Health',
        'Science',
        'Business',
        'Work',
        'Culture',
        'Tenderly',
        'Food',
        'Heated'
    ];

    const news = [
        {
            author: { name: 'Jimmy Morgan' },
            txt: 'A card can be used to display content related to a single subject. The content can consist of multiple elements of varying types and',
            date: new Date().toDateString(),
            claps: 122,
            title: 'A simple card only containing a',
            img: img1
        },
        {
            author: { name: 'Jimmy Morgan' },
            txt: 'A card can be used to display content related to a single subject. The content can consist of multiple elements of varying types and',
            date: new Date().toDateString(),
            claps: 122,
            title: 'A simple card only containing a',
            img: img5
        },
        {
            author: { name: 'Jimmy Morgan' },
            txt: 'A card can be used to display content related to a single subject. The content can consist of multiple elements of varying types and',
            date: new Date().toDateString(),
            claps: 122,
            title: 'A simple card only containing a',
            img: img6
        },
        {
            author: { name: 'Jimmy Morgan' },
            txt: 'A card can be used to display content related to a single subject. The content can consist of multiple elements of varying types and',
            date: new Date().toDateString(),
            claps: 122,
            title: 'A simple card only containing a',
            img: img2
        },
        {
            author: { name: 'Jimmy Morgan' },
            txt: 'A card can be used to display content related to a single subject. The content can consist of multiple elements of varying types and',
            date: new Date().toDateString(),
            claps: 122,
            title: 'A simple card only containing a',
            img: img3
        },
        {
            author: { name: 'Jimmy Morgan' },
            txt: 'A card can be used to display content related to a single subject. The content can consist of multiple elements of varying types and',
            date: new Date().toDateString(),
            claps: 122,
            title: 'A simple card only containing a',
            img: img4
        },
        {
            author: { name: 'Jimmy Morgan' },
            txt: 'A card can be used to display content related to a single subject. The content can consist of multiple elements of varying types and',
            date: new Date().toDateString(),
            claps: 122,
            title: 'A simple card only containing a',
            img: img7
        }
    ]

    return (
        <div className="HomePage">
            <Layout>
                {/*  */}
                <Header >
                    <MainMenu />
                </Header>
                {/*  */}
                <div className="HomePage-Select">
                    <div className="HomePage-Selections">
                        {categories.map((cat, index) => {
                            return (
                                <Button type="link" className={index == 0 ? 'Active' : ''}>
                                    #{cat}
                                    {index == 0 &&
                                        <Icon type="check-circle" />
                                    }
                                </Button>
                            )
                        })}
                    </div>
                    <div className="HomePage-Search">
                        <form>
                        </form>
                    </div>
                </div>
                {/*  */}
                <Content>
                    <Row type="flex" justify="center">
                        <Col lg={{ span: 24 }}>
                            {/* */}
                            <div>
                            <StackGrid  gutterHeight={10} gutterWidth={10} monitorImagesLoaded={true} columnWidth={300}>
                            {
                                news.map((n, index) => {
                                    return (<NCard key={index} article={n} />)
                                })
                            }
                            </StackGrid>
                            </div>
                        </Col>
                    </Row>
                </Content>
                {/*  */}
                <Footer>
                    <MainFooter />
                </Footer>
            </Layout>
        </div>
    );
}

export default HomePage;
